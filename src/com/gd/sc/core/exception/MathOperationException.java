package com.gd.sc.core.exception;

public class MathOperationException extends RuntimeException {
	public MathOperationException(String message) {
		super(message);
	}
}
