package com.gd.sc.core.computation;

/**
 * The methods in this class will perform basic arithmetic operations. All
 * methods in this class will take two parameters and output a result. The two
 * parameters will be of type float and the output will be of type double. This
 * is to prevent overflows.
 *
 * @author rabboni
 */

public interface BasicMath {

	/**
	 * This method will perform addition operation between two values and return
	 * the result.
	 *
	 * @param firstValue
	 * @param secondValue
	 * @return result
	 */
	public Double add(float firstValue, float secondValue);

	/**
	 * This method will perform the subtraction operation between two values and
	 * return the result.
	 * 
	 * @param firstValue
	 * @param secondValue
	 * @return result
	 */
	public Double subtract(float firstValue, float secondValue);

	/**
	 * This method will perform the multiplication operation between two values
	 * and return the result.
	 * 
	 * @param firstValue
	 * @param secondValue
	 * @return result
	 */
	public Double multiply(float firstValue, float secondValue);

	/**
	 * This method will perform the division operation between two values and
	 * return the result.
	 * 
	 * @param firstValue
	 * @param secondValue
	 * @return result
	 */
	public Double divide(float firstValue, float secondValue);

}
