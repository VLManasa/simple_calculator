package com.gd.sc.ui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.gd.sc.controller.CalculatorSingleton;
import com.gd.sc.ui.element.DisplayArea;
import com.gd.sc.utils.OperationEnum;

public class OperatorListener implements ActionListener {

	private OperationEnum operator;
	private DisplayArea area;

	public OperatorListener(OperationEnum operator, DisplayArea area) {
		this.operator = operator;
		this.area = area;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		CalculatorSingleton.getInstanceof().update(operator);
		area.update();
	}

}
