package com.gd.sc.ui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.gd.sc.controller.CalculatorSingleton;
import com.gd.sc.ui.element.DisplayArea;
import com.gd.sc.utils.NumberEnum;

public class NumericListener implements ActionListener {

	private NumberEnum number;
	private DisplayArea area;

	public NumericListener(NumberEnum number, DisplayArea area) {
		this.number = number;
		this.area = area;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		CalculatorSingleton.getInstanceof().update(number);
		area.update();
	}

}
