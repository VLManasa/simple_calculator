package com.gd.sc.utils;

public class Constant {
	public static final String TITLE = "Simple Calculator";

	// these values can change
	public static final int BUTTON_WIDTH = 70;
	public static final int BUTTON_HEIGHT = 50;

	// these values cannot change
	private static final int TOP_MARGIN = 30;
	private static final int ROWS = 4;
	private static final int NUMPAD_COLS = 3;
	private static final int OPPAD_COLS = 2;

	// these values will be calculated based on the preceding values
	public static final int NUMPAD_HEIGHT = BUTTON_HEIGHT * ROWS;
	public static final int NUMPAD_WIDTH = BUTTON_WIDTH * NUMPAD_COLS;

	public static final int OPPAD_HEIGHT = BUTTON_HEIGHT * ROWS;
	public static final int OPPAD_WIDTH = BUTTON_WIDTH * OPPAD_COLS;

	public static final int HISTORY_WIDTH = NUMPAD_WIDTH + OPPAD_WIDTH;
	public static final int HISTORY_HEIGHT = BUTTON_HEIGHT;

	public static final int OPERATOR_WIDTH = BUTTON_WIDTH;
	public static final int OPERATOR_HEIGHT = BUTTON_HEIGHT;

	public static final int INPUT_WIDTH = HISTORY_WIDTH - OPERATOR_WIDTH;
	public static final int INPUT_HEIGHT = OPERATOR_HEIGHT;

	public static final int DISPLAY_HEIGHT = OPERATOR_HEIGHT + HISTORY_HEIGHT;
	public static final int DISPLAY_WIDTH = HISTORY_WIDTH;

	public static final int WINDOW_WIDTH = DISPLAY_WIDTH;
	public static final int WINDOW_HEIGHT = DISPLAY_HEIGHT + NUMPAD_HEIGHT + TOP_MARGIN;

	// unicode characters for the operator buttons
	public static final char DECIMAL = '.';
	public static final char ADD = '\u002B';
	public static final char SUBTRACT = '\u2212';
	public static final char DIVIDE = '\u00F7';
	public static final char BACK = '\u25C4';
	public static final char EQUAL = '=';
	public static final char MULTIPLY = '\u00D7';
	public static final char SIGN = '\u00B1';
	public static final char CLEAR_HISTORY = 'H';
	public static final char CLEAR_INPUT = 'I';

}
